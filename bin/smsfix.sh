#!/usr/bin/env bash
DOCKER_CMD=$(which docker)
if [ -z $DOCKER_CMD ]; then
	echo "Command 'docker' not found, please install docker."
	exit 1
fi

DATABASE=${1:-behat_template}
USER_ID=${2:-2419}
$DOCKER_CMD exec -i -e MYSQL_PWD=root webdoc_webdocmysqlserver_1 mysql -uroot --protocol=tcp --database=${DATABASE} -e "UPDATE Tbl_User set SMSAuthenticationCode = '214411685df4b274ad56ac718898c335' WHERE UserId = ${USER_ID};"
echo "SMS code set to 123456 for UserId: ${USER_ID} on database ${DATABASE}"