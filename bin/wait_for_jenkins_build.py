#!/usr/bin/env python3
"""
    Wait for Jenkins to finish building the current branch.
    This script will recurse into Jenkins jobs to find a job that has the same
    name as the branch and with the same remote source as you.
    Depending on your Jenkins setup and your local remote name this script might
    fail. Try to use origin as your remote name.

    TODO: Move poll interval to a config value
"""
import subprocess
import sys
import xml.etree.ElementTree as ET
import argparse
import configparser
import getpass

from pathlib import Path
from functools import cache
from os import makedirs
from os.path import isfile
from time import sleep
from urllib.parse import quote_plus

import requests

def main():
    """
        Main
    """
    arguments = parse_arguments()

    if not is_git_repo():
        print("You are not in a git repository.")
        sys.exit(1)

    if not is_configured():
        setup_configuration()

    branch = get_branch_name()
    job = find_job_for_branch_recursively(quote_plus(branch))
    if not job:
        print("Could not find a job for {} with branch {} on {}".format(
            get_git_remote_push_url(),
            branch,
            get_config_value('url'))
        )
        sys.exit(1)

    if arguments.verbose > 0:
        print("Will query {} for job information.".format(job.get("url")))

    build = get_latest_build_for_job(job)
    if arguments.verbose > 0:
        print("Latest build is {}".format(build.get("url")))

    status = get_build_status(build)
    if status:
        print("This build is already done with status {}!".format(status))
    else:
        poll_build(build)

@cache
def parse_arguments():
    """Parse arguments from the commandline"""
    parser = argparse.ArgumentParser()
    parser.add_argument("--verbose", "-v", action="count", default=0, help="Print more information")

    return parser.parse_args()

def is_git_repo() -> bool:
    """Check if you are inside a git repository"""
    try:
        is_inside_work_tree = subprocess.check_output(
            ["git", "rev-parse", "--is-inside-work-tree"],
            stderr=subprocess.PIPE,
            encoding="utf-8"
        )
        return "true" in is_inside_work_tree
    except subprocess.CalledProcessError:
        return False

def is_configured() -> bool:
    """Check if this script is configured"""
    return isfile(get_config_path())

def get_config_path() -> Path:
    """Return path to config file"""
    return Path("~/.config/wait_for_jenkins_build/config").expanduser()

def setup_configuration() -> None:
    """Perform a first time configration setup"""
    print("Performing first time setup.")
    url = input("What is the Jenkins URL? Must start with http(s) and have a trailing slash. ")
    user = input("What is your username? ")
    password = getpass.getpass("What is your password? ")

    config_path = get_config_path()
    print("Configuration will be stored in {}".format(config_path))

    config = configparser.ConfigParser()
    config.add_section('jenkins')
    config.set('jenkins', 'url', url)
    config.set('jenkins', 'user', user)
    config.set('jenkins', 'password', password)

    makedirs(config_path.parent, mode=0o755, exist_ok=True)
    with open(config_path, "w") as configfile:
        config.write(configfile)

def get_config_value(key: str) -> str:
    """Read a key from the configration"""
    config = read_config()

    return config.get('jenkins', key)

@cache
def read_config():
    """Read and parse the configuration from filesystem"""
    config = configparser.ConfigParser()
    config.read(get_config_path())

    return config

def get_branch_name() -> str:
    """Get branch name from git"""
    return subprocess.check_output(
        ["git", "rev-parse", "--abbrev-ref", "HEAD"],
        stderr=subprocess.PIPE,
        encoding="utf-8"
    ).rstrip()

def find_job_for_branch_recursively(branch: str, url=None) -> dict:
    """
    TODO: We need to make sure master branch from devtools finds the right job in jenkins.
    """
    if not url:
        url = get_config_value('url')

    data = get_data_from_jenkins(url)
    for job in data.get('jobs', []):
        if job_is_multibranch(job):
            if get_git_remote_push_url() not in get_remote_sources_from_job(job):
                continue

            return find_job_for_branch_recursively(branch, job['url'])

        if job_is_workflow(job):
            if job['name'] == branch:
                return job

    return dict()

def get_data_from_jenkins(url, endpoint = "api/json"):
    """Make http requests to Jenkins for data"""
    response = requests.get(
        "{}{}".format(url, endpoint),
        auth=(get_config_value('user'), get_config_value('password'))
    )
    response.raise_for_status()

    if "application/xml" in response.headers['Content-Type']:
        return ET.fromstring(response.content)

    if "application/json" in response.headers['Content-Type']:
        return response.json()

    return response.content

def job_is_multibranch(job: list) -> bool:
    """Check if a job is a Multi Branch Project"""
    return job['_class'] == 'org.jenkinsci.plugins.workflow.multibranch.WorkflowMultiBranchProject'

def job_is_workflow(job: list) -> bool:
    """Check if a job is a Workflow job"""
    return job['_class'] == 'org.jenkinsci.plugins.workflow.job.WorkflowJob'

def get_remote_sources_from_job(job) -> list:
    """Query a Jenkins job for it's sources"""
    root = get_data_from_jenkins(job['url'], "config.xml")
    return [e.text for e in root.findall('.//sources/data/jenkins.branch.BranchSource/source/remote')]

@cache
def get_git_remote_push_url() -> str:
    """Get the origin push url from git"""
    return subprocess.check_output(
        ["git", "remote", "get-url", "--push", "origin"],
        stderr=subprocess.PIPE,
        encoding="utf-8"
    ).rstrip()

def get_latest_build_for_job(job: dict) -> dict:
    """Gets the latest build for a job"""
    job_data = get_data_from_jenkins(job['url'])
    return job_data['lastBuild']

def poll_build(build: dict) -> bool:
    """Poll the build until it completes"""
    arguments = parse_arguments()
    print("Will poll build #{} until it is done.".format(build['number']))
    while True:
        status = get_build_status(build)
        if arguments.verbose > 1:
            print("Polled build #{} has status {}.".format(build['number'], status))

        if status:
            break

        sleep(10)

    print("Build finished with {}.".format(status))

def get_build_status(build: dict) -> str:
    """Gets the status of a build"""
    build_data = get_data_from_jenkins(build['url'])
    return build_data['result']

if __name__ == '__main__':
    main()
