#!/usr/bin/env bash
docker stop --time 1 $(docker ps -aq)
docker rm --force --volumes $(docker ps -aq)
docker rmi --force $(docker images -q)
docker network rm $(docker network ls -q)
docker builder prune --all --force
docker system prune --all --volumes --force