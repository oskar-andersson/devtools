#!/usr/bin/env python3
import csv
import re
from typing import Any

matcher = re.compile(r"^\[.+\] PHP (\w+)(?:\:*\s*)(.+) in (.+) on line (\d*)$")
stacktrace_matcher = re.compile(r"^\[.+\] PHP\s+(\d+\. .*|Stack trace:)")

def main() -> None:
    """ Create a Jira CSV file for importing PHP errors into epics.
    One file per level is generated. These files should be imported into separate epics, and will create one story per
    file with all errors in that file as subtasks. """
    print("Parsing error log ..")
    errors_by_type = parse_errors_from_file()
    summary_of_errors(errors_by_type)
    print("Writing Jira CSV files ..")
    write_jira_csv(errors_by_type)


def parse_errors_from_file() -> dict:
    errors_by_type = {}
    with open("php_errors.log") as file:
        checking_for_stacktrace = False
        for line in file:
            if checking_for_stacktrace:
                stacktrace_matches = match_stacktrace(line)
                if stacktrace_matches:
                    stacktrace.append("".join(stacktrace_matches))
                else:
                    list_for_stacktrace_adding[0].append(stacktrace)
                    checking_for_stacktrace = False
                    stacktrace = []

            if not checking_for_stacktrace:
                matches = match_php_error(line)
                if matches and not match_recorded(errors_by_type, matches):
                    add_error_level(errors_by_type, matches)
                    add_file_to_error_level(errors_by_type, matches)
                    add_error_to_file(errors_by_type, matches)

                    error_level = matches[0][0]
                    file_name = matches[0][2]
                    list_for_stacktrace_adding = errors_by_type[error_level][file_name]

                    checking_for_stacktrace = True
                    stacktrace = []

        if checking_for_stacktrace and stacktrace:
            # We probably ended the file with a stacktrace
            list_for_stacktrace_adding[0].append(stacktrace)

    return errors_by_type


def match_stacktrace(line):
    return stacktrace_matcher.findall(line)


def match_php_error(line) -> list[Any]:
    return matcher.findall(line)


def match_recorded(errors_by_type, matches) -> bool:
    try:
        for error in errors_by_type[matches[0][0]][matches[0][2]]:
            if error[1] == int(matches[0][3]):
                if error[0] == matches[0][1]:
                    return True
    except KeyError:
        return False

    return False


def add_error_level(errors_by_type, matches):
    if matches[0][0] not in errors_by_type:
        errors_by_type[matches[0][0]] = {}


def add_file_to_error_level(errors_by_type, matches):
    if matches[0][2] not in errors_by_type[matches[0][0]]:
        errors_by_type[matches[0][0]][matches[0][2]] = []


def add_error_to_file(errors_by_type, matches):
    errors_by_type[matches[0][0]][matches[0][2]].append([matches[0][1], int(matches[0][3])])


def summary_of_errors(errors_by_type):
    print("Summary of messages:")
    file_count = 0
    error_count = 0
    for error_level, data in errors_by_type.items():
        error_amount = sum([len(x) for file, errors in data.items() for x in errors])
        print("  - {}: {} files ({} errors)".format(error_level, len(data), error_amount))
        error_count += error_amount
        file_count += len(data)

    print("{} messages over {} files".format(error_count, file_count))


def write_jira_csv(errors_by_type: dict) -> None:
    for error_level in errors_by_type.keys():
        with open("Jira_{}.csv".format(error_level), "w+") as output:
            writer = csv.writer(output, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)
            write_header(writer)
            write_body(writer, errors_by_type[error_level], error_level)


def write_header(output: csv.writer) -> None:
    output.writerow(["Issue Type", "Labels", "Issue Id", "Parent Id", "Summary", "Description"])


def write_body(output: csv.writer, files, level) -> None:
    issue_id = 0
    for file in files:
        issue_id += 1
        output.writerow(["Story", "PHP_7.2_Errors", issue_id, "", file, ""])

        for error in files[file]:
            error_name = error[0]
            line = error[1]
            if len(error) == 3:
                stacktrace = error[2]
            else:
                stacktrace = ""

            summary = "{}: {} on line {}".format(level, error_name, line)
            description = "{} on line {}\n{}".format(error_name, line, "\n".join(stacktrace))
            output.writerow(["Sub-task", "PHP_7.2_Errors", "", issue_id, summary, description.strip()])


if __name__ == "__main__":
    main()
