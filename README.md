# devtools

This repository contains various useful(ish) scripts I've written or collected.

## Files

### /bin

|Filename|Purpose|Invocation example|
|--------|-------|------------------|
|untilfail|Run a command continuously until it exits with non-zero exit status.|./untilfail phpunit|
|wait_for_jenkins_build.py|Wait for the current branch to finish building in Jenkins. Useful for waiting until finished to do a push.|./wait_for_jenkins_build.py && echo "My branch is now finished!"|
|smsfix.sh|Small script that resets the sms auth code for a user. Defaults to database behat_template and UserId 2419 (fixed_write_user) but can be changed with arguments.|./smsfix.sh *database_name* *user_id*|
|docker-remove-all.sh|Remove docker cache, images, containers, networks, volumes. Everything. Useful mainly for testing building docker images.|./docker-remove-all.sh

### /git-hooks

|Filename|Purpose|
|--------|-------|
|prepare-commit-msg|Fetches the Jira ticket number from the branch name and prepends it to the commit message. Does not run for amends, merges or cherry-picks.

### /zsh-completions
Various zsh completion files. Add this directory to your ~/.zshrc and make sure that compinit is started.

Example:
>fpath=(~/git/devtools/zsh-completions $fpath)
> 
>compinit

#### Completion files
- _runDocker
- _careroom
